---
# Complète ici tes informations au format YAML
# un doute sur la syntaxe ? aide toi du site http://www.yamllint.com/

# Tes informations de profil (INFORMATIONS OBLIGATOIRES)
prenom: Muhammad
nom: Pauvert

# écrit ci-dessous ta présentation en markdown
# un doute sur la syntaxe ? aide toi des ressources disponible sur moodle
---

# Profil

![](https://cdn.discordapp.com/attachments/750013655994728510/1164575604088242346/PhotoMuhammadPAUVERT.jpg)




Je suis PAUVERT Muhammad, j'ai 19 ans et j'habite à Nantes. Je suis en BTS SIO (Services Informatiques aux Organisations) au lycée La Colinière. Je suis diplômé d'un baccalauréat technologique STI2D option ITEC. Plus tard, j'aimerais choisir la spécialité SISR (Solutions d'infrastructures, Systèmes et Réseaux), pour me spécialiser dans le domaine du système et des réseaux.

## Contact
Adresse mail : pauvertmuhammad@gmail.com

Linkedin :[Muhammad PAUVERT](https://www.linkedin.com/in/muhammad-pauvert-631996280)

# Formation
-Bac Technologique STI2D (Sciences et Technologies de l'Industrie et du Développement Durable) - Lycée La Colinière

-BTS SIO (Services Informatiques aux Organisations) - Lycée La Colinière

# Compétence

| Compétence                |                                      |
|---------------------------|--------------------------------------|
| Langages de Programmation | Python, HTML, SQL                    |
| Systèmes d'Exploitation   | Windows, Debian                      |
| Langues                   | Français, Anglais                    |
| Logiciels                 | VirtualBox, Wireshark, Packet Tracer |


## Section informatique
